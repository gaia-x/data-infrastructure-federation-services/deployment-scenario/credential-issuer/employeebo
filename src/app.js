const express = require('express');
const app = express();

app.use(express.json());


const employeeBoRoutes = require('./routes/employeeBoRoutes');

app.use('/api/employee', employeeBoRoutes);

module.exports = app;
