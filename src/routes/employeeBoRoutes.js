const express = require('express');
const router = express.Router();

const employeeBoController = require('../controllers/employeeBoController');


router.post('/VC', employeeBoController.VC);
router.post('/login', employeeBoController.login)
router.post('/form', employeeBoController.form)
module.exports = router;
