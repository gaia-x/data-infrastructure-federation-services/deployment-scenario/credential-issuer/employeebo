const { json } = require('express');
const fs = require('fs');
const path = require('path');

const registryPath = path.join(__dirname, '../data/registry.json');
const vcRegistry = JSON.parse(fs.readFileSync(registryPath, 'utf8'));
const userRegistryPath = path.join(__dirname, '../data/userRegistry.json');


exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const isConnected = await checkConnection(username, password);

    if (isConnected) {
      try {
        const directoryPath = path.join(__dirname, '../data/registry/employee');
        fs.readdir(directoryPath, (err, files) => {
          console.log(files)
          if (err) {
            return res.status(500).send({ error: 'Failed to read directory' });
          }
          const exists = files.some(file => file === (username + ".json"));
          if (exists) {
            fs.readFile(path.join(directoryPath, (username + ".json")), 'utf8', (err, data) => {
              if (err) {
                return res.status(500).send({ error: 'Failed to read file' });
              }
              try {
                return res.status(200).send(data)
              } catch (err) {
                res.status(500).send({ error: 'Failed to parse JSON data' });
              }
            });
          } else {
            res.status(404).send({ error: 'File does not exist' });
          }
        });
      }
      catch (error) {
        res.status(500).send({ error: 'Internal error' });
      }
    } else {
      res.status(403).json({ error: 'Connection failed' });
    }
  } catch (error) {
    console.log(error)
    res.status(500).json({ error: 'Internal server error' });
  }
};

exports.checkFile = (user, res) => {

};
// exports.login = async (req, res) => {
//   try {
//     const { username, password } = req.body;
//     const isConnected = await checkConnection(username, password);

//     if (isConnected) {
//       const vcTypes = findVCTypesForDID(username);
//       res.json({ vcTypes });
//     } else {
//       res.status(403).json({ error: 'Connection failed' });
//     }
//   } catch (error) {
//     res.status(500).json({ error: 'Internal server error' });
//   }
// };

function findVCTypesForDID(did) {
  return Object.keys(vcRegistry).filter(vcType => vcRegistry[vcType].includes(did));
}

async function checkConnection(username, password) {
  try {
    // Make a request to the backOffice API to check the connection
    // const response = await axios.post('http://localhost:3001/api/verify-connection', { username, password });
    // Extract the result from the response data
    // const result = response.data;

    return true;
  } catch (error) {
    console.error('Error checking connection:', error);
    return false;
  }
}

exports.form = async (req, res) => {
  try {
    let transformedData = {};
    console.log(req.body)
    if (req.body['type'] === 'employee') {
      transformedData = {
        type: [req.body.type],
        firstName: req.body['schema:firstName'],
        lastName: req.body['schema:lastName'],
        title: req.body['schema:jobTitle'],
        id: 'didURL'
      };
    } else if (req.body.type === 'legalPerson') {
      // Handle other types
    }

    const folderPath = path.join(__dirname, `../data/registry/${req.body.type}`);
    const fileName = req.body.did + '.json';
    const filePath = path.join(folderPath, fileName);

    fs.writeFile(filePath, JSON.stringify(transformedData, null, 1), (err) => {
      if (err) {
        console.error('Error writing file:', err);

        res.status(500).json({ error: 'Internal server error' });
      } else {
        delete transformedData["type"];
        console.log('File created successfully:', filePath);
        res.status(200).json(transformedData);
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

exports.VC = async (req, res) => {
  try {
    const DID = req.body.DID;
    const VC = req.body.VC;

    if (vcRegistry.hasOwnProperty(VC)) {
      if (vcRegistry[VC].includes(DID)) {
        const response = getDIDInfo(VC, DID);
        res.json(response);
      } else {
        const response = createDIDNotFoundResponse();
        res.json(response);
      }
    } else {
      const response = createVCNotFoundResponse();
      res.json(response);
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

function getDIDInfo(type, did) {
  const filePath = path.join(__dirname, `../data/registry/${type}/${did}.json`);

  if (fs.existsSync(filePath)) {
    return fs.readFileSync(filePath, 'utf8');
  } else {
    return createDashResponse(type);
  }
}

function createDashResponse(type) {
  const shaclFile = path.join(__dirname, `../data/dashShacl/${type}.ttl`);
  return fs.readFileSync(shaclFile, 'utf8');
}

function createDIDNotFoundResponse() {
  return {
    found: false,
    message: 'DID not found'
  };
}

function createVCNotFoundResponse() {
  return {
    found: false,
    message: 'This VC cannot be generated for this DID'
  };
}
